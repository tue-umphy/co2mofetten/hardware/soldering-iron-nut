// clang-format off
use <threads-scad/threads.scad>;
// clang-format on

thread_pitch = 1;
thread_diameter = 15.7;
thread_length = 8.6;
thread_tooth_angle = 30;

hole_diameter = 11;
rim_thickness = 2.4;
nut_diameter = 25;

epsilon = 0.1 * 1;

$fs = $preview ? 3 : 0.5;
$fa = $preview ? 5 : 1;

ScrewHole(outer_diam = thread_diameter,
          height = thread_length,
          pitch = thread_pitch,
          tooth_angle = thread_tooth_angle) difference()
{
  cylinder(d = nut_diameter, h = thread_length + rim_thickness, $fn = 6);
  translate([ 0, 0, -epsilon / 2 ])
    cylinder(d = hole_diameter, h = thread_length + rim_thickness + epsilon);
}
